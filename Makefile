MAKEFLAGS += --no-print-directory

# `make V=1' to have verbose output
ifneq ($(V),1)
Q = @
endif

SUITES := $(wildcard suite_*)
SUITES_CLEAN := $(patsubst %, %_clean_, $(SUITES))

.PHONY:		all clean $(SUITES) $(SUITES_CLEAN)

all:		$(SUITES)

clean:		$(SUITES_CLEAN)

$(SUITES):	# FIXME link to mars library, so it gets build first
		@echo Building in $@
		$(Q)$(MAKE) -C $@

$(SUITES_CLEAN):
		@echo Cleaning in $(subst _clean_,,$@)
		$(Q)$(MAKE) -C $(subst _clean_,,$@) clean

# Use `make print-VARIABLE' to print the value of $(VARIABLE)
print-%:
		@echo $* = "$($*)"

