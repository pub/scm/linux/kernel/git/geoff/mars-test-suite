#include <string.h>
#include <malloc.h>
#include <CUnit/Basic.h>
#include <libspe2.h>
#include <mars/mars.h>
#include "host_mpu_common.h"

extern struct spe_program_handle mpu_task_prog;

static struct mars_context mars, temp_mars;
static struct mars_task_id task_id, temp_task_id;
static struct mars_task_args task_args;
static struct mars_mutex mutex, temp_mutexes[2], *wrong_mutex_ptr;
static struct mars_task_barrier barrier, temp_barriers[2], *wrong_barrier_ptr;
static struct mars_task_event_flag event_flag, temp_event_flags[2], *wrong_event_flag_ptr;
static struct mars_task_queue queue, temp_queues[2], *wrong_queue_ptr;
static int8_t *queue_buffer;

//*****************************************************************************
// MARS context initialize/finalize suite functions
//*****************************************************************************

static void TFP_mars_initialize()
{
	int ret, spe_cnt, i;

	CU_ASSERT_EQUAL_MARS(ret = mars_initialize(&mars, 0), MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_finalize(&mars);

	spe_cnt = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, 0);

	for (i = 0;i <= spe_cnt;i++)
	{
		CU_ASSERT_EQUAL_MARS(ret = mars_initialize(&mars, i), MARS_SUCCESS);
		if (ret == MARS_SUCCESS)
			mars_finalize(&mars);
	}
}

static void TFN_mars_initialize()
{
	int spe_cnt;

	spe_cnt = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, 0);

	CU_ASSERT_EQUAL_MARS(mars_initialize(NULL, 0), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_initialize(NULL, spe_cnt), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_initialize(&mars, -1), MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(mars_initialize(&mars, spe_cnt + 1), MARS_ERROR_PARAMS);
}

static void TFP_mars_finalize()
{
	mars_initialize(&mars, 0);
	CU_ASSERT_EQUAL_MARS(mars_finalize(&mars), MARS_SUCCESS);
}

static void TFN_mars_finalize()
{
	CU_ASSERT_EQUAL_MARS(mars_finalize(NULL), MARS_ERROR_NULL);

	memset(&mars, 0, sizeof(struct mars_context));
	CU_ASSERT_EQUAL_MARS(mars_finalize(&mars), MARS_ERROR_STATE);
}

//*****************************************************************************
// common suite functions
//*****************************************************************************

static int init_common_suite()
{
	return mars_initialize(&mars, 0) != MARS_SUCCESS;
}

static int clean_common_suite()
{
	return mars_finalize(&mars) != MARS_SUCCESS;
}

//*****************************************************************************
// MARS task API suite functions
//*****************************************************************************

static void TFP_mars_task_initialize()
{
	int ret;

	CU_ASSERT_EQUAL_MARS(
		ret = mars_task_initialize(
			&mars,
			&task_id,
			NULL,
			mpu_task_prog.elf_image,
			0),
		MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_task_finalize(&task_id);

	CU_ASSERT_EQUAL_MARS(
		ret = mars_task_initialize(
			&mars,
			&task_id,
			NULL,
			mpu_task_prog.elf_image,
			MARS_TASK_CONTEXT_SAVE_SIZE_MAX),
		MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_task_finalize(&task_id);
}

static void TFN_mars_task_initialize()
{
	CU_ASSERT_EQUAL_MARS(
		mars_task_initialize(
			NULL,
			NULL,
			NULL,
			NULL,
			0),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_initialize(
			NULL,
			NULL,
			"no command",
			mpu_task_prog.elf_image,
			MARS_TASK_CONTEXT_SAVE_SIZE_MAX),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_initialize(
			NULL,
			&task_id,
			NULL,
			NULL,
			0),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_initialize(
			NULL,
			&task_id,
			"no command",
			mpu_task_prog.elf_image,
			MARS_TASK_CONTEXT_SAVE_SIZE_MAX),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_initialize(
			&mars,
			NULL,
			NULL,
			NULL,
			0),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_initialize(
			&mars,
			NULL,
			"no command",
			mpu_task_prog.elf_image,
			MARS_TASK_CONTEXT_SAVE_SIZE_MAX),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_initialize(
			&mars,
			&task_id,
			NULL,
			NULL,
			0),
		MARS_ERROR_NULL);

	memset(&temp_mars, 0, sizeof(temp_mars));
	CU_ASSERT_EQUAL_MARS(
		mars_task_initialize(
			&temp_mars,
			&task_id,
			"no command",
			mpu_task_prog.elf_image,
			MARS_TASK_CONTEXT_SAVE_SIZE_MAX),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_initialize(
			&mars,
			&task_id,
			"no command",
			mpu_task_prog.elf_image,
			1000000000),
		MARS_ERROR_PARAMS);
}

static void TFP_mars_task_finalize()
{
	mars_task_initialize(
		&mars,
		&task_id,
		"no command",
		mpu_task_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	CU_ASSERT_EQUAL_MARS(mars_task_finalize(&task_id), MARS_SUCCESS);
}

static void TFN_mars_task_finalize()
{
	CU_ASSERT_EQUAL_MARS(mars_task_finalize(NULL), MARS_ERROR_NULL);

	memset(&task_id, 0, sizeof(task_id));
	task_id.workload_id = 200;
	CU_ASSERT_EQUAL_MARS(mars_task_finalize(&task_id), MARS_ERROR_PARAMS);
}

static void TFP_mars_task_schedule()
{
	int ret, exit_code;

	mars_task_initialize(
		&mars,
		&task_id,
		"no command",
		mpu_task_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);

	CU_ASSERT_EQUAL_MARS(ret = mars_task_schedule(&task_id, NULL, 0), MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_task_wait(&task_id, &exit_code);

	CU_ASSERT_EQUAL_MARS(ret = mars_task_schedule(&task_id, &task_args, 0), MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_task_wait(&task_id, &exit_code);

	CU_ASSERT_EQUAL_MARS(ret = mars_task_schedule(&task_id, &task_args, 100), MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_task_wait(&task_id, &exit_code);

	CU_ASSERT_EQUAL_MARS(ret = mars_task_schedule(&task_id, &task_args, 255), MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_task_wait(&task_id, &exit_code);

	mars_task_finalize(&task_id);
}

static void TFN_mars_task_schedule()
{
	mars_task_initialize(
		&mars,
		&task_id,
		"no command",
		mpu_task_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);

	CU_ASSERT_EQUAL_MARS(mars_task_schedule(NULL, NULL, 0), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_task_schedule(NULL, &task_args, 0), MARS_ERROR_NULL);

	memset(&temp_task_id, 0, sizeof(temp_task_id));
	temp_task_id.workload_id = 200;
	CU_ASSERT_EQUAL_MARS(mars_task_schedule(&temp_task_id, &task_args, 0),  MARS_ERROR_PARAMS);

	mars_task_finalize(&task_id);
}

static void TFP_mars_task_wait()
{
	int exit_code;

	mars_task_initialize(
		&mars,
		&task_id,
		"sleep",
		mpu_task_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	CU_ASSERT_EQUAL_MARS(mars_task_wait(&task_id, &exit_code), MARS_SUCCESS);

	mars_task_schedule(&task_id, NULL, 0);
	CU_ASSERT_EQUAL_MARS(mars_task_wait(&task_id, &exit_code), MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(mars_task_wait(&task_id, &exit_code), MARS_SUCCESS);

	mars_task_finalize(&task_id);
}

static void TFN_mars_task_wait()
{
	int exit_code;

	CU_ASSERT_EQUAL_MARS(mars_task_wait(NULL, &exit_code), MARS_ERROR_NULL);

	memset(&task_id, 0, sizeof(task_id));
	task_id.workload_id = 200;
	CU_ASSERT_EQUAL_MARS(mars_task_wait(&task_id, &exit_code), MARS_ERROR_PARAMS);

	mars_task_initialize(
		&mars,
		&task_id,
		"sleep",
		mpu_task_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	mars_task_wait(&task_id, &exit_code);
	mars_task_finalize(&task_id);
	//CU_ASSERT_EQUAL_MARS(mars_task_wait(&task_id, &exit_code), MARS_ERROR_STATE);
}

static void TFP_mars_task_try_wait()
{
	int exit_code;

	mars_task_initialize(
		&mars,
		&task_id,
		"sleep",
		mpu_task_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	CU_ASSERT_EQUAL_MARS(mars_task_try_wait(&task_id, &exit_code), MARS_SUCCESS);

	mars_task_wait(&task_id, &exit_code);
	CU_ASSERT_EQUAL_MARS(mars_task_try_wait(&task_id, &exit_code), MARS_SUCCESS);

	mars_task_finalize(&task_id);
}

static void TFN_mars_task_try_wait()
{
	int exit_code;

	CU_ASSERT_EQUAL_MARS(mars_task_try_wait(NULL, NULL), MARS_ERROR_NULL);

	memset(&task_id, 0, sizeof(task_id));
	task_id.workload_id = 200;
	CU_ASSERT_EQUAL_MARS(mars_task_try_wait(&task_id, &exit_code), MARS_ERROR_PARAMS);

	mars_task_initialize(
		&mars,
		&task_id,
		"sleep",
		mpu_task_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);

	mars_task_schedule(&task_id, NULL, 0);
	CU_ASSERT_EQUAL_MARS(mars_task_try_wait(&task_id, &exit_code), MARS_ERROR_BUSY);

	mars_task_wait(&task_id, &exit_code);
	mars_task_finalize(&task_id);
	CU_ASSERT_EQUAL_MARS(mars_task_try_wait(&task_id, &exit_code), MARS_ERROR_STATE);
}

//*****************************************************************************
// MARS mutex API suite functions
//*****************************************************************************

static void TFP_mars_mutex_initialize()
{
	CU_ASSERT_EQUAL_MARS(mars_mutex_initialize(&mutex), MARS_SUCCESS);
}

static void TFN_mars_mutex_initialize()
{
	CU_ASSERT_EQUAL_MARS(mars_mutex_initialize(NULL), MARS_ERROR_NULL);

	wrong_mutex_ptr = (struct mars_mutex*)((char*)temp_mutexes+3);
	CU_ASSERT_EQUAL_MARS(mars_mutex_initialize(wrong_mutex_ptr), MARS_ERROR_ALIGN);
}

static void TFP_mars_mutex_lock()
{
	int ret;

	mars_mutex_initialize(&mutex);

	CU_ASSERT_EQUAL_MARS(ret = mars_mutex_lock(&mutex), MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_mutex_unlock(&mutex);
}

static void TFN_mars_mutex_lock()
{
	mars_mutex_initialize(&mutex);

	CU_ASSERT_EQUAL_MARS(mars_mutex_lock(NULL), MARS_ERROR_NULL);

	wrong_mutex_ptr = (struct mars_mutex*)((char*)temp_mutexes+3);
	CU_ASSERT_EQUAL_MARS(mars_mutex_lock(wrong_mutex_ptr), MARS_ERROR_ALIGN);
}

static void TFP_mars_mutex_unlock()
{
	mars_mutex_initialize(&mutex);

	mars_mutex_lock(&mutex);
	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock(&mutex), MARS_SUCCESS);
}

static void TFN_mars_mutex_unlock()
{
	mars_mutex_initialize(&mutex);

	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock(NULL), MARS_ERROR_NULL);

	wrong_mutex_ptr = (struct mars_mutex*)((char*)temp_mutexes+3);
	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock(wrong_mutex_ptr), MARS_ERROR_ALIGN);

	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock(&mutex), MARS_ERROR_STATE);
}

//*****************************************************************************
// MARS barrier API suite functions
//*****************************************************************************

static void TFP_mars_task_barrier_initialize()
{
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(&mars, &barrier, 10), MARS_SUCCESS);
}

static void TFN_mars_task_barrier_initialize()
{
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(NULL, NULL, 0), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(NULL, NULL, 10), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(NULL, &barrier, 0), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(NULL, &barrier, 10), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(&mars, NULL, 0), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(&mars, NULL, 10), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(&mars, &barrier, 0), MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(&mars, &barrier, 100), MARS_ERROR_LIMIT);

	wrong_barrier_ptr = (struct mars_task_barrier*)((char*)temp_barriers+3);
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(&mars, wrong_barrier_ptr, 10), MARS_ERROR_ALIGN);

	memset(&temp_mars, 0, sizeof(temp_mars));
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(&temp_mars, &barrier, 10), MARS_ERROR_NULL);
}

//*****************************************************************************
// MARS task API suite functions
//*****************************************************************************

static void TFP_mars_task_event_flag_initialize()
{
	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			&event_flag,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			&event_flag,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_MANUAL),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			&event_flag,
			MARS_TASK_EVENT_FLAG_MPU_TO_HOST,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			&event_flag,
			MARS_TASK_EVENT_FLAG_MPU_TO_HOST,
			MARS_TASK_EVENT_FLAG_CLEAR_MANUAL),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			&event_flag,
			MARS_TASK_EVENT_FLAG_MPU_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			&event_flag,
			MARS_TASK_EVENT_FLAG_MPU_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_MANUAL),
		MARS_SUCCESS);
}

static void TFN_mars_task_event_flag_initialize()
{
	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			NULL,
			NULL,
			0,
			0),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			NULL,
			NULL,
			0,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			NULL,
			NULL,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			0),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			NULL,
			NULL,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			NULL,
			&event_flag,
			0,
			0),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			NULL,
			&event_flag,
			0,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			NULL,
			&event_flag,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			0),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			NULL,
			&event_flag,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			NULL,
			0,
			0),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			NULL,
			0,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			NULL,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			0),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			NULL,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			&event_flag,
			0,
			0),
		MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			&event_flag,
			0,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			&event_flag,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			0),
		MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			&event_flag,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			100),
		MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			&event_flag,
			100,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_ERROR_PARAMS);

	wrong_event_flag_ptr = (struct mars_task_event_flag*)((char*)temp_event_flags+3);
	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&mars,
			wrong_event_flag_ptr,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_ERROR_ALIGN);

	memset(&temp_mars, 0, sizeof(temp_mars));
	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			&temp_mars,
			&event_flag,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_ERROR_NULL);
}

//*****************************************************************************
// MARS shared data queue API suite functions
//*****************************************************************************

static int init_queue_suite()
{
	queue_buffer = (int8_t*)memalign(MARS_TASK_QUEUE_ENTRY_ALIGN, 65536);

	return init_common_suite();
}

static int clean_queue_suite()
{
	free(queue_buffer);

	return clean_common_suite();
}

static void TFP_mars_task_queue_initialize()
{
	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			&mars,
			&queue,
			queue_buffer,
			16,
			4096,
			MARS_TASK_QUEUE_HOST_TO_MPU),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			&mars,
			&queue,
			queue_buffer,
			16,
			4096,
			MARS_TASK_QUEUE_MPU_TO_HOST),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			&mars,
			&queue,
			queue_buffer,
			16,
			4096,
			MARS_TASK_QUEUE_MPU_TO_MPU),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			&mars,
			&queue,
			queue_buffer,
			16,
			10,
			MARS_TASK_QUEUE_HOST_TO_MPU),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			&mars,
			&queue,
			queue_buffer,
			16,
			100,
			MARS_TASK_QUEUE_HOST_TO_MPU),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			&mars,
			&queue,
			queue_buffer,
			16,
			256,
			MARS_TASK_QUEUE_HOST_TO_MPU),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			&mars,
			&queue,
			queue_buffer,
			16,
			4095,
			MARS_TASK_QUEUE_HOST_TO_MPU),
		MARS_SUCCESS);
}

static void TFN_mars_task_queue_initialize()
{
	wrong_queue_ptr = (struct mars_task_queue*)((char*)temp_queues+3);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			&mars,
			&queue,
			queue_buffer,
			16,
			4096,
			0),
		MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			&mars,
			&queue,
			queue_buffer,
			16,
			4096,
			100),
		MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			&mars,
			&queue,
			queue_buffer,
			16,
			0,
			MARS_TASK_QUEUE_HOST_TO_MPU),
		MARS_ERROR_PARAMS);
}

#define INFO \
"\
Use 'p' and/or 'n' parameter to set a test suite.\n\
\n\
    'p' - (positive functional tests)\n\
    'n' - (negative functional tests)\n\
\n\
Example:\n\
./black_box_test_1 pn\n"

int main(int argc, char** argv)
{
	int added, exit_code;
	CU_pSuite pInitFinSuite, pTaskSuite, pMutexSuite, pBarrierSuite, pEventFlagSuite,
		pQueueSuite;

	if (argc <= 1)
	{
		printf(INFO);
		return 0;
	}

	printf("\n********************************************************************************\n");
	printf("* Host tests                                                                   *\n");
	printf("********************************************************************************\n");

	positive = (int)(uint64_t)strstr(argv[1], "p");
	negative = (int)(uint64_t)strstr(argv[1], "n");

	if (CU_initialize_registry() != CUE_SUCCESS)
		return CU_get_error();

	// Setup all test suites
	pInitFinSuite = CU_add_suite("MARS context initialize/finalize", NULL, NULL);
	pTaskSuite = CU_add_suite("MARS task API", init_common_suite, clean_common_suite);
	pMutexSuite = CU_add_suite("MARS mutex API", init_common_suite, clean_common_suite);
	pBarrierSuite = CU_add_suite("MARS barrier API", init_common_suite, clean_common_suite);
	pEventFlagSuite = CU_add_suite("MARS event flag API", init_common_suite, clean_common_suite);
	pQueueSuite = CU_add_suite("MARS shared data queue API", init_queue_suite, clean_queue_suite);

	added = pInitFinSuite &&
		pTaskSuite &&
		pMutexSuite &&
		pBarrierSuite &&
		pEventFlagSuite &&
		pQueueSuite;

	if (!added)
	{
		CU_cleanup_registry();
		return CU_get_error();
	}

	// Add all functional tests
	CU_add_test_mars(pInitFinSuite, mars_initialize);
	CU_add_test_mars(pInitFinSuite, mars_finalize);

	CU_add_test_mars(pTaskSuite, mars_task_initialize);
	CU_add_test_mars(pTaskSuite, mars_task_finalize);
	CU_add_test_mars(pTaskSuite, mars_task_schedule);
	CU_add_test_mars(pTaskSuite, mars_task_wait);
	CU_add_test_mars(pTaskSuite, mars_task_try_wait);

	CU_add_test_mars(pMutexSuite, mars_mutex_initialize);
	CU_add_test_mars(pMutexSuite, mars_mutex_lock);
	CU_add_test_mars(pMutexSuite, mars_mutex_unlock);

	CU_add_test_mars(pBarrierSuite, mars_task_barrier_initialize);

	CU_add_test_mars(pEventFlagSuite, mars_task_event_flag_initialize);

	CU_add_test_mars(pQueueSuite, mars_task_queue_initialize);

	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();

	// MPU part

	mars_initialize(&mars, 0);

	spe_test_params.positive = positive;
	spe_test_params.negative = negative;

	mars_task_initialize(
		&mars,
		&spe_test_params.mars_task_schedule_task_id,
		"no command",
		mpu_task_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);

	mars_task_initialize(
		&mars,
		&spe_test_params.mars_task_wait_task_id,
		"sleep",
		mpu_task_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);

	spe_test_params.mutex_ea = (uint64_t)&mutex;
	spe_test_params.barrier_ea = (uint64_t)&barrier;
	spe_test_params.event_flag_ea = (uint64_t)&event_flag;
	spe_test_params.queue_ea = (uint64_t)&queue;
	spe_test_params.queue_buffer_ea = (uint64_t)queue_buffer;

	mars_task_initialize(
		&mars,
		&task_id,
		"spe test",
		mpu_task_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	task_args.type.u64[0] = (uint64_t)&spe_test_params;
	mars_task_schedule(&task_id, &task_args, 0);
	mars_task_wait(&task_id, &exit_code);
	mars_task_finalize(&task_id);

	mars_task_wait(&spe_test_params.mars_task_wait_task_id, &exit_code);
	mars_task_finalize(&spe_test_params.mars_task_wait_task_id);

	mars_task_wait(&spe_test_params.mars_task_schedule_task_id, &exit_code);
	mars_task_finalize(&spe_test_params.mars_task_schedule_task_id);

	mars_finalize(&mars);

	return CU_get_error();
}
