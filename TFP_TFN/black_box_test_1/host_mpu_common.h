#include <string.h>

/** CU_ASSERT_EQUAL override to see return value
 *  Asserts that actual == expected.
 *  Reports failure with no other action.
 */

int act_val, exp_val;
char print_msg[1024], print_msg_ret[30];

#define APPEND_RET(RET) if (act_val == RET) snprintf(print_msg_ret, 30, "%s", #RET);

#define CU_ASSERT_EQUAL_MARS(actual, expected)\
\
	{\
		act_val = (actual);\
		exp_val = (expected);\
		strncpy(print_msg, "CU_ASSERT_EQUAL_MARS(" #actual "," #expected "), function returned ", 1024);\
		APPEND_RET(MARS_SUCCESS)\
		else\
		APPEND_RET(MARS_ERROR_NULL)\
		else\
		APPEND_RET(MARS_ERROR_PARAMS)\
		else\
		APPEND_RET(MARS_ERROR_INTERNAL)\
		else\
		APPEND_RET(MARS_ERROR_MEMORY)\
		else\
		APPEND_RET(MARS_ERROR_ALIGN)\
		else\
		APPEND_RET(MARS_ERROR_LIMIT)\
		else\
		APPEND_RET(MARS_ERROR_STATE)\
		else\
		APPEND_RET(MARS_ERROR_FORMAT)\
		else\
		APPEND_RET(MARS_ERROR_BUSY)\
		else\
		snprintf(print_msg_ret, 30, "%i", act_val);\
		strncat(print_msg, print_msg_ret, 1023);\
		CU_assertImplementation((act_val == exp_val), __LINE__, (print_msg), __FILE__, "", CU_FALSE);\
	}

static int positive, negative;
static char add_test_buffer[128];

#define CU_add_test_mars_pn(PN, SUITE, PREFIX, FUNCTION)\
\
	if (PN)\
	{\
		strncpy(add_test_buffer, #PREFIX, 128);\
		strncat(add_test_buffer, #FUNCTION, 128);\
		if (!CU_add_test(SUITE, add_test_buffer, PREFIX##FUNCTION))\
		{\
			CU_cleanup_registry();\
			return CU_get_error();\
		}\
	}

#define CU_add_test_mars(SUITE, FUNCTION)\
\
	CU_add_test_mars_pn(positive, SUITE, TFP_, FUNCTION)\
	CU_add_test_mars_pn(negative, SUITE, TFN_, FUNCTION)

struct SPE_TEST_PARAMS
{
	int positive;
	int negative;
	struct mars_task_id mars_task_schedule_task_id;
	struct mars_task_id mars_task_wait_task_id;
	uint64_t mutex_ea;
	uint64_t barrier_ea;
	uint64_t event_flag_ea;
	uint64_t queue_ea;
	uint64_t queue_buffer_ea;
};

static struct SPE_TEST_PARAMS spe_test_params;
