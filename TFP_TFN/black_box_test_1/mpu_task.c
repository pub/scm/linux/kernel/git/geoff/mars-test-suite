#include <stdio.h>
#include <unistd.h>
#include <CUnit/Basic.h>
#include "mars/mars.h"
#include "mars/mars_dma.h"
#include "mars/mars_task.h"
#include "host_mpu_common.h"

static struct mars_task_id temp_task_id;
static struct mars_task_args task_args;
static struct mars_mutex mutex, temp_mutexes[2], *wrong_mutex_ptr;
static uint64_t wrong_mutex_ea;
static uint64_t wrong_barrier_ea;
static uint64_t wrong_event_flag_ea;

static void TFP_mars_task_schedule()
{
	int ret, exit_code;

	CU_ASSERT_EQUAL_MARS(
		ret = mars_task_schedule(
			&spe_test_params.mars_task_schedule_task_id,
			NULL,
			0),
		MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_task_wait(&spe_test_params.mars_task_schedule_task_id, &exit_code);

	CU_ASSERT_EQUAL_MARS(
		ret = mars_task_schedule(
			&spe_test_params.mars_task_schedule_task_id,
			&task_args,
			0),
		MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_task_wait(&spe_test_params.mars_task_schedule_task_id, &exit_code);

	CU_ASSERT_EQUAL_MARS(
		ret = mars_task_schedule(
			&spe_test_params.mars_task_schedule_task_id,
			&task_args,
			100),
		MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_task_wait(&spe_test_params.mars_task_schedule_task_id, &exit_code);

	CU_ASSERT_EQUAL_MARS(
		ret = mars_task_schedule(
			&spe_test_params.mars_task_schedule_task_id,
			&task_args,
			255),
		MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_task_wait(&spe_test_params.mars_task_schedule_task_id, &exit_code);
}

static void TFN_mars_task_schedule()
{
	CU_ASSERT_EQUAL_MARS(mars_task_schedule(NULL, NULL, 0), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_task_schedule(NULL, &task_args, 0), MARS_ERROR_NULL);

	memset(&temp_task_id, 0, sizeof(temp_task_id));
	temp_task_id.workload_id = 200;
	CU_ASSERT_EQUAL_MARS(mars_task_schedule(&temp_task_id, &task_args, 0), MARS_ERROR_PARAMS);
}

static void TFP_mars_task_wait()
{
	int exit_code;

	CU_ASSERT_EQUAL_MARS(
		mars_task_wait(
			&spe_test_params.mars_task_wait_task_id,
			&exit_code),
		MARS_SUCCESS);

	mars_task_schedule(&spe_test_params.mars_task_wait_task_id, NULL, 0);
	CU_ASSERT_EQUAL_MARS(
		mars_task_wait(
			&spe_test_params.mars_task_wait_task_id,
			&exit_code),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_wait(
			&spe_test_params.mars_task_wait_task_id,
			&exit_code),
		MARS_SUCCESS);
}

static void TFN_mars_task_wait()
{
	CU_ASSERT_EQUAL_MARS(mars_task_wait(NULL, NULL), MARS_ERROR_NULL);

	//memset(&temp_task_id, 0, sizeof(temp_task_id));
	//temp_task_id.workload_id = 200;
	//CU_ASSERT_EQUAL_MARS(mars_task_wait(&temp_task_id), MARS_ERROR_PARAMS);
}

static void TFP_mars_task_try_wait()
{
	int exit_code;

	CU_ASSERT_EQUAL_MARS(
		mars_task_try_wait(
			&spe_test_params.mars_task_wait_task_id,
			&exit_code),
		MARS_SUCCESS);

	mars_task_schedule(&spe_test_params.mars_task_wait_task_id, NULL, 0);
	mars_task_wait(&spe_test_params.mars_task_wait_task_id, &exit_code);
	CU_ASSERT_EQUAL_MARS(
		mars_task_try_wait(
			&spe_test_params.mars_task_wait_task_id,
			&exit_code),
		MARS_SUCCESS);
}

static void TFN_mars_task_try_wait()
{
	int exit_code;

	CU_ASSERT_EQUAL_MARS(mars_task_try_wait(NULL, NULL), MARS_ERROR_NULL);

	memset(&temp_task_id, 0, sizeof(temp_task_id));
	temp_task_id.workload_id = 200;
	CU_ASSERT_EQUAL_MARS(mars_task_try_wait(&temp_task_id, &exit_code), MARS_ERROR_PARAMS);

	mars_task_schedule(&spe_test_params.mars_task_wait_task_id, NULL, 0);
	CU_ASSERT_EQUAL_MARS(
		mars_task_try_wait(
			&spe_test_params.mars_task_wait_task_id,
			&exit_code),
		MARS_ERROR_BUSY);
}

static void TFP_mars_mutex_initialize()
{
	CU_ASSERT_EQUAL_MARS(mars_mutex_initialize(spe_test_params.mutex_ea), MARS_SUCCESS);
}

static void TFN_mars_mutex_initialize()
{
	CU_ASSERT_EQUAL_MARS(mars_mutex_initialize(0), MARS_ERROR_NULL);

	wrong_mutex_ea = spe_test_params.mutex_ea+3;
	CU_ASSERT_EQUAL_MARS(mars_mutex_initialize(wrong_mutex_ea), MARS_ERROR_ALIGN);
}

static void TFP_mars_mutex_lock()
{
	int ret;

	mars_mutex_initialize(spe_test_params.mutex_ea);

	CU_ASSERT_EQUAL_MARS(ret = mars_mutex_lock(spe_test_params.mutex_ea), MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_mutex_unlock(spe_test_params.mutex_ea);
}

static void TFN_mars_mutex_lock()
{
	CU_ASSERT_EQUAL_MARS(mars_mutex_lock(0), MARS_ERROR_NULL);

	wrong_mutex_ea = spe_test_params.mutex_ea+3;
	CU_ASSERT_EQUAL_MARS(mars_mutex_lock(wrong_mutex_ea), MARS_ERROR_ALIGN);
}

static void TFP_mars_mutex_unlock()
{
	mars_mutex_initialize(spe_test_params.mutex_ea);

	mars_mutex_lock(spe_test_params.mutex_ea);
	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock(spe_test_params.mutex_ea), MARS_SUCCESS);
}

static void TFN_mars_mutex_unlock()
{
	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock(0), MARS_ERROR_NULL);

	wrong_mutex_ea = spe_test_params.mutex_ea+3;
	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock(wrong_mutex_ea), MARS_ERROR_ALIGN);

	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock(spe_test_params.mutex_ea), MARS_ERROR_STATE);
}

static void TFP_mars_mutex_lock_get()
{
	int ret;

	mars_mutex_initialize(spe_test_params.mutex_ea);

	CU_ASSERT_EQUAL_MARS(ret = mars_mutex_lock_get(spe_test_params.mutex_ea, &mutex), MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_mutex_unlock(spe_test_params.mutex_ea);
}

static void TFN_mars_mutex_lock_get()
{
	CU_ASSERT_EQUAL_MARS(mars_mutex_lock_get(0, NULL), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_mutex_lock_get(0, &mutex), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_mutex_lock_get(spe_test_params.mutex_ea, NULL), MARS_ERROR_NULL);

	wrong_mutex_ptr = (struct mars_mutex*)((char*)temp_mutexes+3);
	CU_ASSERT_EQUAL_MARS(mars_mutex_lock_get(spe_test_params.mutex_ea, wrong_mutex_ptr), MARS_ERROR_ALIGN);

	wrong_mutex_ea = spe_test_params.mutex_ea+3;
	CU_ASSERT_EQUAL_MARS(mars_mutex_lock_get(wrong_mutex_ea, &mutex), MARS_ERROR_ALIGN);
}

static void TFP_mars_mutex_unlock_put()
{
	mars_mutex_initialize(spe_test_params.mutex_ea);

	mars_mutex_lock_get(spe_test_params.mutex_ea, &mutex);
	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock_put(spe_test_params.mutex_ea, &mutex), MARS_SUCCESS);
}

static void TFN_mars_mutex_unlock_put()
{
	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock_put(0, NULL), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock_put(0, &mutex), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock_put(spe_test_params.mutex_ea, NULL), MARS_ERROR_NULL);

	wrong_mutex_ptr = (struct mars_mutex*)((char*)temp_mutexes+3);
	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock_put(spe_test_params.mutex_ea, wrong_mutex_ptr), MARS_ERROR_ALIGN);

	wrong_mutex_ea = spe_test_params.mutex_ea+3;
	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock_put(wrong_mutex_ea, &mutex), MARS_ERROR_ALIGN);

	CU_ASSERT_EQUAL_MARS(mars_mutex_unlock_put(spe_test_params.mutex_ea, &mutex), MARS_ERROR_STATE);
}

static void TFP_mars_task_barrier_initialize()
{
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(spe_test_params.barrier_ea, 10), MARS_SUCCESS);
}

static void TFN_mars_task_barrier_initialize()
{
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(0, 0), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(0, 10), MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(spe_test_params.barrier_ea, 0), MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(spe_test_params.barrier_ea, 100), MARS_ERROR_LIMIT);

	wrong_barrier_ea = spe_test_params.barrier_ea+3;
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_initialize(wrong_barrier_ea, 10), MARS_ERROR_ALIGN);
}

static void TFP_mars_task_barrier_notify()
{
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_notify(spe_test_params.barrier_ea), MARS_SUCCESS);
}

static void TFN_mars_task_barrier_notify()
{
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_notify(0), MARS_ERROR_NULL);

	wrong_barrier_ea = spe_test_params.barrier_ea+3;
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_notify(wrong_barrier_ea), MARS_ERROR_ALIGN);
}

static void TFP_mars_task_barrier_wait()
{
	mars_task_barrier_initialize(spe_test_params.barrier_ea, 1);
	mars_task_barrier_notify(spe_test_params.barrier_ea);
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_wait(spe_test_params.barrier_ea), MARS_SUCCESS);
}

static void TFN_mars_task_barrier_wait()
{
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_wait(0), MARS_ERROR_NULL);

	wrong_barrier_ea = spe_test_params.barrier_ea+3;
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_wait(wrong_barrier_ea), MARS_ERROR_ALIGN);
}

static void TFP_mars_task_barrier_try_wait()
{
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_try_wait(spe_test_params.barrier_ea), MARS_SUCCESS);
}

static void TFN_mars_task_barrier_try_wait()
{
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_try_wait(0), MARS_ERROR_NULL);

	wrong_barrier_ea = spe_test_params.barrier_ea+3;
	CU_ASSERT_EQUAL_MARS(mars_task_barrier_try_wait(wrong_barrier_ea), MARS_ERROR_ALIGN);
}

static void TFP_mars_task_event_flag_initialize()
{
	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			spe_test_params.event_flag_ea,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			spe_test_params.event_flag_ea,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_MANUAL),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			spe_test_params.event_flag_ea,
			MARS_TASK_EVENT_FLAG_MPU_TO_HOST,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			spe_test_params.event_flag_ea,
			MARS_TASK_EVENT_FLAG_MPU_TO_HOST,
			MARS_TASK_EVENT_FLAG_CLEAR_MANUAL),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			spe_test_params.event_flag_ea,
			MARS_TASK_EVENT_FLAG_MPU_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			spe_test_params.event_flag_ea,
			MARS_TASK_EVENT_FLAG_MPU_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_MANUAL),
		MARS_SUCCESS);
}

static void TFN_mars_task_event_flag_initialize()
{
	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			0,
			0,
			0),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			0,
			0,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			0,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			0),
		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			0,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),

		MARS_ERROR_NULL);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			spe_test_params.event_flag_ea,
			0,
			0),
		MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			spe_test_params.event_flag_ea,
			0,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			spe_test_params.event_flag_ea,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			0),
		MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			spe_test_params.event_flag_ea,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			100),
		MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			spe_test_params.event_flag_ea,
			100,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_ERROR_PARAMS);

	wrong_event_flag_ea = spe_test_params.event_flag_ea+3;
	CU_ASSERT_EQUAL_MARS(
		mars_task_event_flag_initialize(
			wrong_event_flag_ea,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO),
		MARS_ERROR_ALIGN);
}

static void TFP_mars_task_queue_initialize()
{
	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			spe_test_params.queue_ea,
			spe_test_params.queue_buffer_ea,
			16,
			4096,
			MARS_TASK_QUEUE_HOST_TO_MPU),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			spe_test_params.queue_ea,
			spe_test_params.queue_buffer_ea,
			16,
			4096,
			MARS_TASK_QUEUE_MPU_TO_HOST),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			spe_test_params.queue_ea,
			spe_test_params.queue_buffer_ea,
			16,
			4096,
			MARS_TASK_QUEUE_MPU_TO_MPU),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			spe_test_params.queue_ea,
			spe_test_params.queue_buffer_ea,
			16,
			10,
			MARS_TASK_QUEUE_HOST_TO_MPU),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			spe_test_params.queue_ea,
			spe_test_params.queue_buffer_ea,
			16,
			100,
			MARS_TASK_QUEUE_HOST_TO_MPU),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			spe_test_params.queue_ea,
			spe_test_params.queue_buffer_ea,
			16,
			256,
			MARS_TASK_QUEUE_HOST_TO_MPU),
		MARS_SUCCESS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			spe_test_params.queue_ea,
			spe_test_params.queue_buffer_ea,
			16,
			4095,
			MARS_TASK_QUEUE_HOST_TO_MPU),
		MARS_SUCCESS);
}

static void TFN_mars_task_queue_initialize()
{
	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			spe_test_params.queue_ea,
			spe_test_params.queue_buffer_ea,
			16,
			4096,
			0),
		MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			spe_test_params.queue_ea,
			spe_test_params.queue_buffer_ea,
			16,
			4096,
			100),
		MARS_ERROR_PARAMS);

	CU_ASSERT_EQUAL_MARS(
		mars_task_queue_initialize(
			spe_test_params.queue_ea,
			spe_test_params.queue_buffer_ea,
			16,
			0,
			MARS_TASK_QUEUE_HOST_TO_MPU),
		MARS_ERROR_PARAMS);
}

int mars_task_main(const struct mars_task_args *task_args)
{
	int added;
	CU_pSuite pTaskSuite, pMutexSuite, pBarrierSuite, pEventFlagSuite, pQueueSuite;

	if (strcmp(mars_task_get_name(), "sleep") == 0)
	{
		sleep(2);
		return 0;
	}

	if (strcmp(mars_task_get_name(), "spe test") != 0)
		return 0;

	printf("\n********************************************************************************\n");
	printf("* MPU tests                                                                    *\n");
	printf("********************************************************************************\n");

	mars_dma_get_and_wait(&spe_test_params, task_args->type.u64[0], sizeof(spe_test_params), 0);

	positive = spe_test_params.positive;
	negative = spe_test_params.negative;

	if (CU_initialize_registry() != CUE_SUCCESS)
		return CU_get_error();

	// Setup all test suites
	pTaskSuite = CU_add_suite("MARS task API", NULL, NULL);
	pMutexSuite = CU_add_suite("MARS mutex API", NULL, NULL);
	pBarrierSuite = CU_add_suite("MARS barrier API", NULL, NULL);
	pEventFlagSuite = CU_add_suite("MARS event flag API", NULL, NULL);
	pQueueSuite = CU_add_suite("MARS shared data queue API", NULL, NULL);

	added = pTaskSuite &&
		pMutexSuite &&
		pBarrierSuite &&
		pEventFlagSuite &&
		pQueueSuite;

	if (!added)
	{
		CU_cleanup_registry();
		return CU_get_error();
	}

	// Add all functional tests
	CU_add_test_mars(pTaskSuite, mars_task_schedule);
	CU_add_test_mars(pTaskSuite, mars_task_wait);
	CU_add_test_mars(pTaskSuite, mars_task_try_wait);

	CU_add_test_mars(pMutexSuite, mars_mutex_initialize);
	CU_add_test_mars(pMutexSuite, mars_mutex_lock);
	CU_add_test_mars(pMutexSuite, mars_mutex_unlock);
	CU_add_test_mars(pMutexSuite, mars_mutex_lock_get);
	CU_add_test_mars(pMutexSuite, mars_mutex_unlock_put);

	CU_add_test_mars(pBarrierSuite, mars_task_barrier_initialize);
	CU_add_test_mars(pBarrierSuite, mars_task_barrier_notify);
	CU_add_test_mars(pBarrierSuite, mars_task_barrier_wait);
	CU_add_test_mars(pBarrierSuite, mars_task_barrier_try_wait)

	CU_add_test_mars(pEventFlagSuite, mars_task_event_flag_initialize);

	CU_add_test_mars(pQueueSuite, mars_task_queue_initialize);

	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();

	return CU_get_error();
}
