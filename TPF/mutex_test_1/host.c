#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <libspe2.h>
#include <unistd.h>
#include <inttypes.h>
#include "mars/mars.h"
#include "host_mutex.h"

#define INFO \
"\n\
MARS Mutex Test\n\
--------------------\n\
This program measures a Mutex reservation lost event probability.\n\
\n\
You can get more precise results by overriding loop count by number parameter.\n\
Example:\n\
./mutex_benchmark 500\n\
\n"

#define MIN_TASK_COUNT      1
#define MAX_TASK_COUNT      16
#define MIN_CALC_LOOP_COUNT 1
#define MAX_CALC_LOOP_COUNT 65536
#define DEFAULT_LOCK_COUNT  256

struct TASK_RESULT
{
	uint64_t result;
	uint64_t unused;
} __attribute__((aligned(16)));

extern struct spe_program_handle task_spe_prog;

static struct mars_context mars;
static struct mars_task_params task_params;
static struct mars_task_id task_ids[MAX_TASK_COUNT];
static struct mars_task_args task_args;
static struct mars_mutex mutex;
static struct TASK_RESULT tasks_results[MAX_TASK_COUNT];

#define MUTEX &mutex
#include "host_mpu_common.h"

void print_border_line()
{
	int i;

	printf("|");
	for (i = 0;i < 157;i++)
		printf("-");
	printf("|\n");
}

void print_table_header()
{
	int calc_loop_count, i;

	printf("\n");

	print_border_line();

	printf("| Mutex reservation lost event percentage - Host , MPU");
	for (i = 0;i < 104;i++)
		printf(" ");
	printf("|\n");

	print_border_line();

	printf("| loop count |");
	for (calc_loop_count = MIN_CALC_LOOP_COUNT;calc_loop_count <= MAX_CALC_LOOP_COUNT;calc_loop_count*=4)
		printf("| %13i ",calc_loop_count);
	printf("|\n");

	printf("|------------||");	
	for (calc_loop_count = MIN_CALC_LOOP_COUNT;calc_loop_count <= MAX_CALC_LOOP_COUNT;calc_loop_count*=4)
		printf("  Host    MPU  |");
	printf("\n");

	printf("| task count ||");
	for (calc_loop_count = MIN_CALC_LOOP_COUNT;calc_loop_count <= MAX_CALC_LOOP_COUNT;calc_loop_count*=4)
		printf("   [%%]    [%%]  |");
	printf("\n");

	print_border_line();
}

int main(int argc, char** argv)
{
	int task, task_count, lock_count, calc_loop_count;



	printf(INFO);



	if (argc == 2)
		lock_count = atol(argv[1]);
	else
	{
		lock_count = DEFAULT_LOCK_COUNT;

		printf("Choosing default lock count = %i\n\n", DEFAULT_LOCK_COUNT);
	}



	mars_initialize(&mars, NULL);



	mars_mutex_initialize(&mutex);



	/********************/
	/* Host to MPU part */
	/********************/

	print_table_header();

	for (task_count = MIN_TASK_COUNT;task_count <= MAX_TASK_COUNT;task_count++)
	{
		printf("| %10i ||",task_count);

		for (calc_loop_count = MIN_CALC_LOOP_COUNT;calc_loop_count <= MAX_CALC_LOOP_COUNT;calc_loop_count*=4)
		{
			mutex.pad[0] = lock_count;
			mutex.pad[1] = calc_loop_count;

			for (task = 0;task < task_count;task++)
			{
				task_params.name = "MPU Task";
				task_params.elf_image = task_spe_prog.elf_image;
				task_params.context_save_size = MARS_TASK_CONTEXT_SAVE_SIZE_MAX;

				mars_task_initialize(&mars, &task_ids[task], &task_params);
			}

			for (task = 0;task < task_count;task++)
			{
				task_args.type.u64[0] = (uint64_t)&mutex;
				task_args.type.u64[1] = (uint64_t)&tasks_results[task];
				mars_task_schedule(&task_ids[task], &task_args, 0);
			}

			reservation_lost = 0;

			process_locking(lock_count, calc_loop_count);

			printf(" %6.3f", 100.0*reservation_lost/lock_count);

			reservation_lost = 0;

			for (task = 0;task < task_count;task++)
			{
				mars_task_wait(&task_ids[task]);

				mars_task_finalize(&task_ids[task]);

				reservation_lost += tasks_results[task].result;
			}

			printf(" %6.3f |", 100.0*reservation_lost/(task_count*lock_count));
		}

		printf("\n");
	}

	print_border_line();



	mars_finalize(&mars);



	return 0;
}


