static int64_t calc_counter = 0;

static void process_locking(int64_t lock_count, int64_t calc_loop_count)
{
	int64_t lock, calc;

	for (lock = 0;lock < lock_count;lock++)
	{
                mars_mutex_lock(MUTEX);

		for (calc = 0;calc < calc_loop_count;calc++)
		{
			if ((calc % 2) == 0)
				calc_counter += calc;
			else
				calc_counter -= calc;
		}

		mars_mutex_unlock(MUTEX);
	}
}
