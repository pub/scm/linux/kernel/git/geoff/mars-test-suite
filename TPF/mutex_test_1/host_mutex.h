#include "mars_mutex_types.h"
#include "mars/mars_atomic.h"
#include "mars/mars_error.h"
#include "mars/mars_debug.h"

int64_t reservation_lost __attribute__((aligned(16))) = 0;

int mars_mutex_initialize(struct mars_mutex *mutex)
{
	MARS_CHECK_RET(mutex, MARS_ERROR_NULL);
	MARS_CHECK_RET(((uintptr_t)mutex & MARS_MUTEX_ALIGN_MASK) == 0,
			MARS_ERROR_ALIGN);

	mutex->lock = MARS_MUTEX_UNLOCKED;

	return MARS_SUCCESS;
}

int mars_mutex_lock(struct mars_mutex *mutex)
{
	MARS_CHECK_RET(mutex, MARS_ERROR_NULL);
	MARS_CHECK_RET(((uintptr_t)mutex & MARS_MUTEX_ALIGN_MASK) == 0,
			MARS_ERROR_ALIGN);

	int64_t local_reservation_lost = -1;

	do {
		local_reservation_lost++;

		do {
		} while (atomic_get(&mutex->lock) == MARS_MUTEX_LOCKED);
	} while (!atomic_put(&mutex->lock, MARS_MUTEX_LOCKED));

	reservation_lost += local_reservation_lost;

	atomic_sync();

	return MARS_SUCCESS;
}

int mars_mutex_unlock(struct mars_mutex *mutex)
{
	MARS_CHECK_RET(mutex, MARS_ERROR_NULL);
	MARS_CHECK_RET(((uintptr_t)mutex & MARS_MUTEX_ALIGN_MASK) == 0,
			MARS_ERROR_ALIGN);
	MARS_CHECK_RET(mutex->lock == MARS_MUTEX_LOCKED, MARS_ERROR_STATE);

	mutex->lock = MARS_MUTEX_UNLOCKED;

	return MARS_SUCCESS;
}
