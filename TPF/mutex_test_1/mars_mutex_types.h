#ifndef MARS_MUTEX_TYPES_H
#define MARS_MUTEX_TYPES_H

/**
 * \file
 * \ingroup group_mars_mutex
 * \brief MARS Mutex Types
 */

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

/**
 * \ingroup group_mars_mutex
 * \brief Size of mutex structure
 */
#define MARS_MUTEX_SIZE				128

/**
 * \ingroup group_mars_mutex
 * \brief Alignment of mutex structure
 */
#define MARS_MUTEX_ALIGN			128

/**
 * \ingroup group_mars_mutex
 * \brief Alignment mask of mutex structure
 */
#define MARS_MUTEX_ALIGN_MASK			0x7f

/**
 * \ingroup group_mars_mutex
 * \brief Value of mutex lock variable in locked state
 */
#define MARS_MUTEX_LOCKED			0x1

/**
 * \ingroup group_mars_mutex
 * \brief Value of mutex lock variable in unlocked state
 */
#define MARS_MUTEX_UNLOCKED			0x0

/**
 * \ingroup group_mars_mutex
 * \brief MARS mutex structure
 *
 * An instance of this structure must be created when using the MARS Mutex API.
 *
 * If allocating a memory area for this structure, make sure to allocate
 * a memory area that is aligned to \ref MARS_MUTEX_ALIGN bytes
 * and of size \ref MARS_MUTEX_SIZE bytes.
 */
struct mars_mutex {
	uint32_t lock;
	uint32_t pad[31];
} __attribute__((aligned(MARS_MUTEX_ALIGN)));

#if defined(__cplusplus)
}
#endif

#endif
