/#include "mars_mutex_types.h"
#include "mars/mars_atomic.h"
#include "mars/mars_dma.h"
#include "mars/mars_error.h"
#include "mars/mars_debug.h"

static struct mars_mutex mutex;

int64_t reservation_lost __attribute__((aligned(16))) = 0;

int mars_mutex_initialize(uint64_t mutex_ea)
{
	MARS_CHECK_RET(mutex_ea, MARS_ERROR_NULL);
	MARS_CHECK_RET((mutex_ea & MARS_MUTEX_ALIGN_MASK) == 0,
			MARS_ERROR_ALIGN);

	mutex.lock = MARS_MUTEX_UNLOCKED;

	mars_dma_put_and_wait(&mutex, mutex_ea, sizeof(struct mars_mutex),
		MARS_DMA_TAG);

	return MARS_SUCCESS;
}

int mars_mutex_lock_get(uint64_t mutex_ea, struct mars_mutex *mutex)
{
	MARS_CHECK_RET(mutex_ea, MARS_ERROR_NULL);
	MARS_CHECK_RET(mutex, MARS_ERROR_NULL);
	MARS_CHECK_RET((mutex_ea & MARS_MUTEX_ALIGN_MASK) == 0,
			MARS_ERROR_ALIGN);
	MARS_CHECK_RET(((uintptr_t)mutex & MARS_MUTEX_ALIGN_MASK) == 0,
			MARS_ERROR_ALIGN);

	int status;
	int64_t local_reservation_lost = -1;

	atomic_event_setup();

	do {
		local_reservation_lost++;

		atomic_get(mutex, mutex_ea);

		if (mutex->lock == MARS_MUTEX_LOCKED) {
			atomic_event_wait();
			status = 1;
		} else {
			mutex->lock = MARS_MUTEX_LOCKED;
			status = atomic_put(mutex, mutex_ea);
		}
	} while (status);

	atomic_event_restore();
//if (local_reservation_lost > 0)printf("MPU: local_reservation_lost = %llu\n",local_reservation_lost);
	reservation_lost += local_reservation_lost;

	return MARS_SUCCESS;
}

int mars_mutex_unlock_put(uint64_t mutex_ea, struct mars_mutex *mutex)
{
	MARS_CHECK_RET(mutex_ea, MARS_ERROR_NULL);
	MARS_CHECK_RET(mutex, MARS_ERROR_NULL);
	MARS_CHECK_RET((mutex_ea & MARS_MUTEX_ALIGN_MASK) == 0,
			MARS_ERROR_ALIGN);
	MARS_CHECK_RET(((uintptr_t)mutex & MARS_MUTEX_ALIGN_MASK) == 0,
			MARS_ERROR_ALIGN);
	MARS_CHECK_RET(mutex->lock == MARS_MUTEX_LOCKED, MARS_ERROR_STATE);

	mutex->lock = MARS_MUTEX_UNLOCKED;

	mars_dma_put_and_wait(mutex, mutex_ea, sizeof(struct mars_mutex),
		MARS_DMA_TAG);

	return MARS_SUCCESS;
}

int mars_mutex_lock(uint64_t mutex_ea)
{
	return mars_mutex_lock_get(mutex_ea, &mutex);
}

int mars_mutex_unlock(uint64_t mutex_ea)
{
	return mars_mutex_unlock_put(mutex_ea, &mutex);
}
