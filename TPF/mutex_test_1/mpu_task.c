#include <stdio.h>
#include <stdlib.h>
#include "mars/mars.h"
#include "mpu_mutex.h"

static uint64_t mutex_ea;
static uint64_t result_ea;

static struct mars_mutex mutex;

static uint32_t lock_count;
static uint32_t calc_loop_count;

#define MUTEX mutex_ea
#include "host_mpu_common.h"

int mars_task_main(const struct mars_task_args *task_args)
{
	mutex_ea = task_args->type.u64[0];
	result_ea = task_args->type.u64[1];

	mars_mutex_lock_get(mutex_ea, &mutex);

	lock_count = mutex.pad[0];
	calc_loop_count = mutex.pad[1];

	mars_mutex_unlock(mutex_ea);

	process_locking(lock_count, calc_loop_count);

	mars_dma_put_and_wait(&reservation_lost, result_ea, 8, 0);

	return 0;
}
