## config.mk

PPU_CROSS = ppu-
PPU_CC = $(PPU_CROSS)gcc
PPU_CPPFLAGS =
PPU_CFLAGS = $(CFLAGS) -Wall -O3 -funroll-loops
PPU_LDFLAGS = $(LDFLAGS) -lspe2
PPU_EMBEDSPU = $(PPU_CROSS)embedspu

SPU_CROSS = spu-
SPU_CC = $(SPU_CROSS)gcc
SPU_CFLAGS = -Wall -O3 -funroll-loops
SPU_LDFLAGS = \
	-Wl,-N -Wl,-gc-sections -Wl,--section-start,.init=0x10000 \
	-Wl,--entry,mars_entry -Wl,-u,mars_entry
