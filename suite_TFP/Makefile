## Makefile

include ../config.mk

# `make V=1' to have verbose output
ifneq ($(V),1)
Q = @
endif

MARS_LIB_SRC	= ../../mars-src/src
MARS_LIB_INC	= ../../mars-src/include

PPU_CFLAGS	+= $(CFLAGS) -I$(MARS_LIB_INC)/common -I$(MARS_LIB_INC)/host -m64
SPU_CFLAGS	+= $(CFLAGS) -I$(MARS_LIB_INC)/common -I$(MARS_LIB_INC)/mpu

PPU_LDFLAGS	+= $(LFLAGS) $(MARS_LIB_SRC)/host/lib/.libs/libmars.a $(MARS_LIB_SRC)/host/lib/mars-kernel.eo -lcunit
SPU_LDFLAGS	+= $(LFLAGS) $(MARS_LIB_SRC)/mpu/lib/libmars.a

TARGETS		= test_host test_mpu


.PHONY:		all clean

all:		$(TARGETS)

clean:
		$(Q)$(RM) tests.h $(TARGETS) *.o *.eo *~


HOSTSRCS := $(wildcard *_host.c)
HOSTOBJS := $(patsubst %.c, %.o, $(HOSTSRCS))
HOSTOBJS += test_mpu.eo

MPUSRCS := $(wildcard *_mpu.c)
MPUOBJS := $(patsubst %.c, %.o, $(MPUSRCS))

# Make hackery to create the list of tests
TESTSRCS := $(wildcard test_*.c)
TESTS := $(subst test_,,$(TESTSRCS))
TESTS := $(subst _host.c,,$(TESTS))
TESTS := $(subst _mpu.c,,$(TESTS))
TESTS := $(sort $(TESTS))
LPAREN := (
RPAREN := )
TESTDEFS := $(addprefix DEF_TEST$(LPAREN), $(addsuffix $(RPAREN), $(TESTS)))

tests.h:	$(TESTSRCS)
		@echo "GEN    $@"
		$(Q)echo "$(TESTDEFS)" > tests.h

test_host:	$(HOSTOBJS)
		@echo "PPU_LD $@"
		$(Q)$(PPU_CC) -o $@ $(HOSTOBJS) $(PPU_CFLAGS) $(PPU_LDFLAGS)

test_mpu:	$(MPUOBJS)
		@echo "SPU_LD  $@"
		$(Q)$(SPU_CC) -o $@ $(MPUOBJS) $(SPU_CFLAGS) $(SPU_LDFLAGS)

%_host.o:	%_host.c
		@echo "PPU_CC $@"
		$(Q)$(PPU_CC) $(PPU_CFLAGS) -c -o $@ $<

%_mpu.o:	%_mpu.c
		@echo "SPU_CC  $@"
		$(Q)$(SPU_CC) $(SPU_CFLAGS) -c -o $@ $<

%_mpu.eo:	%_mpu
		@echo "PPU_EMBEDSPU  $@"
		$(Q)$(PPU_EMBEDSPU) $(PPU_CFLAGS) $< $< $@
		

suite_host.o:	tests.h
suite_mpu.o:	tests.h


# For debugging
%_host.i:	%_host.c
		@echo "PPU_CC $<"
		$(Q)$(PPU_CC) $(PPU_CFLAGS) -E -o $@ $<

%_mpu.i:	%_mpu.c
		@echo "SPU_CC $<"
		$(Q)$(SPU_CC) $(SPU_CFLAGS) -E -o $@ $<

suite_host.i:	tests.h
suite_mpu.i:	tests.h


# Use `make print-VARIABLE' to print the value of $(VARIABLE)
print-%:
		@echo $* = "$($*)"
