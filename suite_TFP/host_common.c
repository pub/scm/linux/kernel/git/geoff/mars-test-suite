#include "host_common.h"

#define APPEND_SIG(SIG) if (sig == SIG) snprintf(sig_text, 30, "%s", #SIG);

void append_sig(char* buffer, int buf_len, int sig)
{
	char sig_text[30];
	APPEND_SIG(SIGHUP)
	else
	APPEND_SIG(SIGINT)
	else
	APPEND_SIG(SIGQUIT)
	else
	APPEND_SIG(SIGILL)
	else
	APPEND_SIG(SIGTRAP)
	else
	APPEND_SIG(SIGABRT)
	else
	APPEND_SIG(SIGIOT)
	else
	APPEND_SIG(SIGBUS)
	else
	APPEND_SIG(SIGFPE)
	else
	APPEND_SIG(SIGKILL)
	else
	APPEND_SIG(SIGUSR1)
	else
	APPEND_SIG(SIGSEGV)
	else
	APPEND_SIG(SIGUSR2)
	else
	APPEND_SIG(SIGPIPE)
	else
	APPEND_SIG(SIGALRM)
	else
	APPEND_SIG(SIGTERM)
	else
	APPEND_SIG(SIGSTKFLT)
	else
	APPEND_SIG(SIGCHLD)
	else
	APPEND_SIG(SIGCONT)
	else
	APPEND_SIG(SIGSTOP)
	else
	APPEND_SIG(SIGTSTP)
	else
	APPEND_SIG(SIGTTIN)
	else
	APPEND_SIG(SIGTTOU)
	else
	APPEND_SIG(SIGURG)
	else
	APPEND_SIG(SIGXCPU)
	else
	APPEND_SIG(SIGXFSZ)
	else
	APPEND_SIG(SIGVTALRM)
	else
	APPEND_SIG(SIGPROF)
	else
	APPEND_SIG(SIGWINCH)
	else
	APPEND_SIG(SIGIO)
	else
	APPEND_SIG(SIGPOLL)
	else
/*
	APPEND_SIG(SIGLOST)
	else
*/
	APPEND_SIG(SIGPWR)
	else
	APPEND_SIG(SIGSYS)
	else
	APPEND_SIG(SIGUNUSED)
	else

/* These should not be considered constants from userland.  */
	APPEND_SIG(SIGRTMIN)
	else
	APPEND_SIG(SIGRTMAX)
	else
	snprintf(sig_text, 30, "%i", sig);
	strncat(buffer, sig_text, buf_len-1);
}

