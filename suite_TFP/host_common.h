#include <libspe2.h>
#include <signal.h>
#include <pthread.h>

extern struct spe_program_handle test_mpu;

void append_sig(char* buffer, int buf_len, int sig);

#define THREAD_PROC_WITH_SIGNAL_HANDLER_ARG(proc_name, proc_code, process_arg)\
\
void proc_name##_signal_handler(int arg)\
{\
	char print_msg[1024] = "ERROR: " #proc_name "_signal_handler was called with signal value: ";\
	append_sig(print_msg, 1024, arg);\
	CU_assertImplementation(0, __LINE__, (print_msg), __FILE__, "", CU_FALSE);\
\
	pthread_exit(NULL);\
}\
\
void* proc_name(void* arg)\
{\
	signal(SIGSEGV, proc_name##_signal_handler);\
\
	proc_code\
\
	signal(SIGSEGV, NULL);\
\
	if (process_arg)\
		*((int*)arg) = 1;\
\
	return NULL;\
}

#define THREAD_PROC_WITH_SIGNAL_HANDLER(proc_name, proc_code) THREAD_PROC_WITH_SIGNAL_HANDLER_ARG(proc_name, proc_code, 0)

#define TEST_PROC(proc_name, time_elapse_ms, proc_code)\
\
static pthread_t proc_name##_thread;\
void* proc_name##_thread_proc(void* arg);\
\
void proc_name(void)\
{\
	struct timespec req, rem;\
	char print_msg[1024];\
	int i;\
	volatile int proc_name##_thread_returned = 0;\
\
	pthread_create(&proc_name##_thread, NULL, proc_name##_thread_proc, (void*)&proc_name##_thread_returned);\
\
	req.tv_sec = 0;\
	req.tv_nsec = 1000000;\
\
	for (i = 0;i < time_elapse_ms && !proc_name##_thread_returned;i++)\
		nanosleep(&req, &rem);\
\
	if (!proc_name##_thread_returned)\
	{\
		strncpy(print_msg, "ERROR: " #proc_name " didn't return within reserved time, possible stalled", 1024);\
		CU_assertImplementation(0, __LINE__, (print_msg), __FILE__, "", CU_FALSE);\
	}\
}\
THREAD_PROC_WITH_SIGNAL_HANDLER_ARG(proc_name##_thread_proc, proc_code, 1)

