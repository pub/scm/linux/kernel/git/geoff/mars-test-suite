#include <string.h>
#include <stdio.h>
#include "mars/mars.h"
#include "host_mpu_common.h"

void append_ret_dec(char* buffer, int buf_len, int ret)
{
	char ret_text[30];
	snprintf(ret_text, 30, "%i", ret);
	strncat(buffer, ret_text, buf_len-1);
}

void append_ret_hex(char* buffer, int buf_len, int ret)
{
	char ret_text[30];
	snprintf(ret_text, 30, "%X", ret);
	strncat(buffer, ret_text, buf_len-1);
}

#define APPEND_RET_MARS(RET) if (ret == RET) snprintf(ret_text, 30, "%s", #RET);

void append_ret_mars(char* buffer, int buf_len, int ret)
{
	char ret_text[30];
	APPEND_RET_MARS(MARS_SUCCESS)
	else
	APPEND_RET_MARS(MARS_ERROR_NULL)
	else
	APPEND_RET_MARS(MARS_ERROR_PARAMS)
	else
	APPEND_RET_MARS(MARS_ERROR_INTERNAL)
	else
	APPEND_RET_MARS(MARS_ERROR_MEMORY)
	else
	APPEND_RET_MARS(MARS_ERROR_ALIGN)
	else
	APPEND_RET_MARS(MARS_ERROR_LIMIT)
	else
	APPEND_RET_MARS(MARS_ERROR_STATE)
	else
	APPEND_RET_MARS(MARS_ERROR_FORMAT)
	else
	APPEND_RET_MARS(MARS_ERROR_BUSY)
	else
	snprintf(ret_text, 30, "%i", ret);
	strncat(buffer, ret_text, buf_len-1);
}
