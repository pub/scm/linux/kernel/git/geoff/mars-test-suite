#include <string.h>

#define TEST_ITEM_017_RET_VAL 0xABCDEF01

/** CU_ASSERT_EQUAL override to see return value
 *  Asserts that actual == expected.
 *  Reports failure with no other action.
 */

int act_val, exp_val;
char print_msg[1024];

void append_ret_dec(char* buffer, int buf_len, int ret);
void append_ret_hex(char* buffer, int buf_len, int ret);
void append_ret_mars(char* buffer, int buf_len, int ret);

#define CU_ASSERT_EQUAL_RET(actual, expected, suffix, ret_format)\
\
	{\
		act_val = (actual);\
		exp_val = (expected);\
		strncpy(print_msg, "CU_ASSERT_EQUAL_" #suffix "(" #actual "," #expected "), first assertion argument value is ", 1024);\
		append_ret_##ret_format(print_msg, 1024, act_val);\
		CU_assertImplementation((act_val == exp_val), __LINE__, (print_msg), __FILE__, "", CU_FALSE);\
	}

#define CU_ASSERT_EQUAL_VAL(actual, expected) CU_ASSERT_EQUAL_RET(actual, expected, VAL, dec)
#define CU_ASSERT_EQUAL_VALX(actual, expected) CU_ASSERT_EQUAL_RET(actual, expected, VALX, hex)
#define CU_ASSERT_EQUAL_MARS(actual, expected) CU_ASSERT_EQUAL_RET(actual, expected, MARS, mars)
