#include <CUnit/Basic.h>
#include <CUnit/Console.h>
#include <CUnit/Automated.h>

#include <stdlib.h>
#include <stdio.h>
#include "host_mpu_common.c"
#include "host_common.c"


#define DEF_TEST(test)	extern void run_ ## test (void);
#include "tests.h"
#undef DEF_TEST


static CU_TestInfo tests[] = {
#define DEF_TEST(test)	{ #test, run_ ## test },
#include "tests.h"
#undef DEF_TEST
	{ NULL, }
};

static int init_suite_TPF(void)
{
	return 0;
}

static int clean_suite_TPF(void)
{
	return 0;
}

static CU_SuiteInfo suites[] = {
	{ "Suite TPF", init_suite_TPF, clean_suite_TPF, tests },
	{ NULL, }
};

static void print_help(void) {
	printf("-a\tautomated generating xml\n");
	printf("-b\tbasic output to stdout\n");
	printf("\t\t--n\tnormal\n");
	printf("\t\t--v\tverbose\n");
	printf("\t\t--s\tsilent\n");
	printf("-i\tinteractive\n");
	printf("-help\tthis help\n");
}

int main(int argc, char *argv[])
{
	CU_BasicRunMode basic_mode = CU_BRM_NORMAL;
	int run_mode = 1; /* 0 = automated, 1 = basic, 2 = console */

	/* initialize the CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry())
		return CU_get_error();

	/* add a suite to the registry */
	if (CUE_SUCCESS != CU_register_suites(suites))
		return CU_get_error();

	/* parse command line arguments */
	if (argc > 1) {
		int i;
		for (i = 1; i < argc; ++i) {
			int length = strlen(argv[i]);
			const char * p = argv[i];
			if (length >= 2 && p[0] == '-') {
				++p;
				if (length >= 3 && p[0] == '-') {
					++p;
					if (strcmp(p, "n") == 0) basic_mode = CU_BRM_NORMAL;
					else if (strcmp(argv[i], "v") == 0) basic_mode = CU_BRM_VERBOSE;
					else if (strcmp(argv[i], "s") == 0) basic_mode = CU_BRM_SILENT;
				}
				else {
					if (strcmp(p, "a") == 0) run_mode = 0;
					else if (strcmp(p, "b") == 0) run_mode = 1;
					else if (strcmp(p, "i") == 0) run_mode = 2;
					else if (strcmp(p, "help") == 0) { print_help(); exit(0); }
				}
			}
		}
	}

	/* set basic mode */
	CU_basic_set_mode(basic_mode);

	switch (run_mode) {
		case 0:	/* Run all tests giving xml output */
			CU_automated_run_tests();
			break;
		case 1: /* Run all tests with stdout output */
			CU_basic_run_tests();
			break;
		case 2: /* Run in console mode */
			CU_console_run_tests();
			break;
	}

	CU_cleanup_registry();

	return CU_get_error();
}


