#include "mars/mars.h"
#include <string.h>
#include "host_mpu_common.h"

int mars_task_main(const struct mars_task_args *task_args)
{
	if (strcmp(mars_task_get_name(), "017") == 0)
		return TEST_ITEM_017_RET_VAL;

	return 0;
}
