#include <CUnit/CUnit.h>
#include "host_mpu_common.h"
#include "host_common.h"
#include "mars/mars.h"

static struct mars_context mars;

TEST_PROC(run_001, 1000,

	int ret;

	CU_ASSERT_EQUAL_MARS(ret = mars_initialize(&mars, 1), MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_finalize(&mars);
)
