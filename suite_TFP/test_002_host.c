#include <CUnit/CUnit.h>
#include "host_mpu_common.h"
#include "host_common.h"
#include <libspe2.h>
#include "mars/mars.h"

static struct mars_context mars;

TEST_PROC(run_002, 1000,

	int ret;
	int spe_cnt = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	CU_ASSERT_EQUAL_MARS(ret = mars_initialize(&mars, spe_cnt), MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_finalize(&mars);
)


