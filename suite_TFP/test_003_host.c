#include <CUnit/CUnit.h>
#include "host_mpu_common.h"
#include "host_common.h"
#include "mars/mars.h"

#define THREAD_COUNT 16

static pthread_t thread_handles[THREAD_COUNT];
static struct mars_context mars_contexts[THREAD_COUNT];

THREAD_PROC_WITH_SIGNAL_HANDLER(run_003_work_thread_proc,

	int ret;

	CU_ASSERT_EQUAL_MARS(
		ret = mars_initialize(
			(struct mars_context*)arg,
			1),
		MARS_SUCCESS);
	if (ret == MARS_SUCCESS)
		mars_finalize((struct mars_context*)arg);
)

TEST_PROC(run_003, 1000,

	int i;

	for (i = 0;i < THREAD_COUNT;i++)
		pthread_create(&thread_handles[i], NULL, run_003_work_thread_proc, (void*)&mars_contexts[i]);

	for (i = 0;i < THREAD_COUNT;i++)
		pthread_join(thread_handles[i], NULL);
)
