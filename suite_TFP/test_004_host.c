#include <CUnit/CUnit.h>
#include "host_mpu_common.h"
#include "host_common.h"
#include <libspe2.h>
#include "mars/mars.h"

static struct mars_context mars;

TEST_PROC(run_004, 1000,

	int spe_cnt = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	mars_initialize(&mars, spe_cnt);

	CU_ASSERT_EQUAL_MARS(mars_finalize(&mars), MARS_SUCCESS);
)


