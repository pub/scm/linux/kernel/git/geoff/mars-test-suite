#include <CUnit/CUnit.h>
#include "host_mpu_common.h"
#include "host_common.h"
#include "mars/mars.h"

#define THREAD_COUNT 16

static pthread_t thread_handles[THREAD_COUNT];
static struct mars_context mars_contexts[THREAD_COUNT];

void* run_005_work_thread_proc(void* arg)
{
	mars_initialize(&mars_contexts[(uint64_t)arg], 1);

	CU_ASSERT_EQUAL_MARS(
		mars_finalize(&mars_contexts[(uint64_t)arg]),
		MARS_SUCCESS);

	return NULL;
}

TEST_PROC(run_005, 1000,

	int i;

	for (i = 0;i < THREAD_COUNT;i++)
		pthread_create(&thread_handles[i], NULL, run_005_work_thread_proc, (void*)(intptr_t)i);

	for (i = 0;i < THREAD_COUNT;i++)
		pthread_join(thread_handles[i], NULL);
)
