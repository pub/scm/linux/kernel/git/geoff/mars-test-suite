#include <CUnit/CUnit.h>
#include "host_mpu_common.h"
#include "host_common.h"
#include "mars/mars.h"

#define THREAD_COUNT 16

static pthread_t thread_handles_1[THREAD_COUNT];
static pthread_t thread_handles_2[THREAD_COUNT];
static struct mars_context mars_contexts_1[THREAD_COUNT];
static struct mars_context mars_contexts_2[THREAD_COUNT];

void* run_006_work_thread_proc(void* arg)
{
	CU_ASSERT_EQUAL_MARS(
		mars_initialize((struct mars_context*)arg, 1),
		MARS_SUCCESS);
 
	CU_ASSERT_EQUAL_MARS(
		mars_finalize((struct mars_context*)arg),
		MARS_SUCCESS);

	return NULL;
}

TEST_PROC(run_006, 1000,

	int i;

	for (i = 0;i < THREAD_COUNT;i++)
		pthread_create(
			&thread_handles_1[i],
			NULL,
			run_006_work_thread_proc,
			(void*)&mars_contexts_1[i]);

	for (i = 0;i < THREAD_COUNT;i++)
		pthread_create(
			&thread_handles_2[i],
			NULL,
			run_006_work_thread_proc,
			(void*)&mars_contexts_2[i]);

	for (i = 0;i < THREAD_COUNT;i++)
		pthread_join(thread_handles_1[i], NULL);

	for (i = 0;i < THREAD_COUNT;i++)
		pthread_join(thread_handles_2[i], NULL);
)
