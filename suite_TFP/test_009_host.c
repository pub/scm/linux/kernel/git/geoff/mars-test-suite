#include <CUnit/CUnit.h>
#include "host_mpu_common.h"
#include "host_common.h"
#include "mars/mars.h"
#include "mars/mars_task.h"

#define MAX_TASKS 256

static struct mars_context mars;
static struct mars_task_id task_ids[MAX_TASKS];

TEST_PROC(run_009, 1000,

	int i;
	int spe_cnt = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	CU_ASSERT(spe_cnt <= MAX_TASKS);

	if (spe_cnt > MAX_TASKS)
		spe_cnt = MAX_TASKS;

	mars_initialize(&mars, spe_cnt);

	for (i = 0;i < spe_cnt;i++)
	{
		CU_ASSERT_EQUAL_MARS(
			mars_task_initialize(
				&mars,
				&task_ids[i],
				NULL,
				test_mpu.elf_image,
				0),
			MARS_SUCCESS);
	}

	for (i = 0;i < spe_cnt;i++)
		mars_task_finalize(&task_ids[i]);

	mars_finalize(&mars);
)
