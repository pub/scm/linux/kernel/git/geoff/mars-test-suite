#include <CUnit/CUnit.h>
#include "host_mpu_common.h"
#include "host_common.h"
#include "mars/mars.h"
#include "mars/mars_task.h"
#include "mars/mars_workload_types.h"

static struct mars_context mars;
static struct mars_task_id task_ids[MARS_WORKLOAD_MAX];

TEST_PROC(run_011, 1000,

	int i;

	mars_initialize(&mars, 0);

	for (i = 0;i < MARS_WORKLOAD_MAX;i++)
	{
		CU_ASSERT_EQUAL_MARS(
			mars_task_initialize(
				&mars,
				&task_ids[i],
				NULL,
				test_mpu.elf_image,
				0),
			MARS_SUCCESS);
	}

	for (i = 0;i < MARS_WORKLOAD_MAX;i++)
		mars_task_finalize(&task_ids[i]);

	mars_finalize(&mars);
)
