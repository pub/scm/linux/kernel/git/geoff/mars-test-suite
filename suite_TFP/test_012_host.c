#include <CUnit/CUnit.h>
#include "host_mpu_common.h"
#include "host_common.h"
#include <libspe2.h>
#include "mars/mars.h"

#define TASK_COUNT 16

static struct mars_context mars;
static struct mars_task_id task_ids[TASK_COUNT];
static pthread_t thread_handles[TASK_COUNT];

THREAD_PROC_WITH_SIGNAL_HANDLER(run_012_work_thread_proc,

	int ret;

	CU_ASSERT_EQUAL_MARS(
		ret = mars_task_initialize(
			&mars,
			(struct mars_task_id*)arg,
			NULL,
			test_mpu.elf_image,
			0),
		MARS_SUCCESS);

	if (ret == MARS_SUCCESS)
		mars_task_finalize((struct mars_task_id*)arg);
)

TEST_PROC(run_012, 1000,

	int i;
	int spe_cnt = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	mars_initialize(&mars, spe_cnt);

	for (i = 0;i < TASK_COUNT;i++)
		pthread_create(&thread_handles[i], NULL, run_012_work_thread_proc, (void*)&task_ids[i]);

	for (i = 0;i < TASK_COUNT;i++)
		pthread_join(thread_handles[i], NULL);

	mars_finalize(&mars);
)
