#include <CUnit/CUnit.h>
#include "host_mpu_common.h"
#include "host_common.h"
#include <libspe2.h>
#include "mars/mars.h"

static struct mars_context mars;
static struct mars_task_id task_id;

TEST_PROC(run_013, 1000,

	mars_initialize(&mars, 1);

	mars_task_initialize(&mars, &task_id, NULL, test_mpu.elf_image, 0);
	CU_ASSERT_EQUAL_MARS(mars_task_finalize(&task_id), MARS_SUCCESS);

	mars_finalize(&mars);
)
