#include <CUnit/CUnit.h>
#include "host_mpu_common.h"
#include "host_common.h"
#include <libspe2.h>
#include "mars/mars.h"

static struct mars_context mars;
static struct mars_task_id task_id;

TEST_PROC(run_014, 1000,

	int exit_code;

	mars_initialize(&mars, 1);

	mars_task_initialize(&mars, &task_id, NULL, test_mpu.elf_image, 0);
	mars_task_schedule(&task_id, NULL, 0);
	mars_task_wait(&task_id, &exit_code);
	CU_ASSERT_EQUAL_MARS(mars_task_finalize(&task_id), MARS_SUCCESS);

	mars_finalize(&mars);
)
