#include <CUnit/Basic.h>

#include <stdlib.h>
#include <stdio.h>


#define DEF_TEST(test)	extern void run_ ## test (void);
#include "tests.h"
#undef DEF_TEST


static CU_TestInfo tests[] = {
#define DEF_TEST(test)	{ #test, run_ ## test },
#include "tests.h"
#undef DEF_TEST
	{ NULL, }
};

static int init_suite_TPF(void)
{
	return 0;
}

static int clean_suite_TPF(void)
{
	return 0;
}

static CU_SuiteInfo suites[] = {
	{ "Suite TPF", init_suite_TPF, clean_suite_TPF, tests },
	{ NULL, }
};

int main(int argc, char *argv[])
{
	/* initialize the CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry())
		return CU_get_error();

	/* add a suite to the registry */
	if (CUE_SUCCESS != CU_register_suites(suites))
		return CU_get_error();

	/* Run all tests using the CUnit Basic interface */
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();

	return CU_get_error();
}
